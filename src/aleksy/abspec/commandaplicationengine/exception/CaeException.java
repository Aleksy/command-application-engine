package aleksy.abspec.commandaplicationengine.exception;

/**
 * PCAE main exception
 */
public class CaeException extends Exception {
   /**
    * Constructor
    * @param message of the exception
    */
   public CaeException(String message) {
      super("Command Application Engine Exception: " + message);
   }
}
