package aleksy.abspec.commandaplicationengine.constants;

/**
 * Basic constants values
 */
public class BasicConstants {
   /**
    * Name
    */
   public static final String NAME = "Command Application Engine";
   /**
    * Version
    */
   public static final String VERSION = "3.0.0-pre";
   /**
    * Author
    */
   public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
}
