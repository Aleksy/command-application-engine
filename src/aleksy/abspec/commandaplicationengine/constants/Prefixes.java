package aleksy.abspec.commandaplicationengine.constants;

/**
 * Default prefixes
 */
public class Prefixes {
   /**
    * Default argument prefix
    */
   public static final String DEFAULT_ARGUMENT_PREFIX = "-";
   /**
    * Default flag prefix
    */
   public static final String DEFAULT_FLAG_PREFIX = "/";
   /**
    * Default subcommand prefix
    */
   public static final String DEFAULT_SUBCOMMAND_PREFIX = ":";
}
