package aleksy.abspec.commandaplicationengine.annotation;

import aleksy.abspec.commandaplicationengine.model.AbstractCommand;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Command annotation can be added above the class name.
 * It should be added to the classes which extend the
 * {@link AbstractCommand}. By the name value the
 * command can be recognized by the engine.
 * Description value is used to display the help options
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Command {
   /**
    * Name of the command
    * @return name
    */
   String name();

   /**
    * Description of the command
    * @return description
    */
   String description();
}
