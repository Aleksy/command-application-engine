package aleksy.abspec.commandaplicationengine.model;

/**
 * Example of usage builder
 */
public class ExampleOfUsageBuilder {
   private AbstractCommand command;
   private StringBuilder stringBuilder;

   /**
    * The constructor
    *
    * @param command to add the example
    */
   ExampleOfUsageBuilder(AbstractCommand command) {
      this.command = command;
      this.stringBuilder = new StringBuilder();
   }

   /**
    * Adds an argument to the example
    * @param name of argument
    * @param value of argument
    * @return builder
    */
   public ExampleOfUsageBuilder argument(String name, String value) {
      stringBuilder
         .append(" ")
         .append(command.getArgumentPrefix())
         .append(name)
         .append(" ")
         .append(value);
      return this;
   }

   /**
    * Adds a flag to the example
    * @param name of flag
    * @return builder
    */
   public ExampleOfUsageBuilder flag(String name) {
      stringBuilder
         .append(" ")
         .append(command.getFlagPrefix())
         .append(name);
      return this;
   }

   /**
    * Adds a subcommand to the example
    * @param name of subcommand
    * @return builder
    */
   public ExampleOfUsageBuilder subcommand(String name) {
      stringBuilder
         .append(" ")
         .append(command.getSubcommandPrefix())
         .append(name);
      return this;
   }

   /**
    * Adds a description to the example and saves full example to the command
    * @param description of example
    */
   public void description(String description) {
      stringBuilder.append(" ==> ").append(description);
      command.addExampleOfUsage(stringBuilder.toString());
   }
}
