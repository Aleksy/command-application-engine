package aleksy.abspec.commandaplicationengine.model;

import aleksy.abspec.commandaplicationengine.logic.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract command class
 */
public abstract class AbstractCommand {
   private String applicationCommandName;
   private String command;
   private String subcommand;
   private String argumentPrefix;
   private String flagPrefix;
   private String subcommandPrefix;
   private Map<String, String> arguments;
   private List<String> flags;
   private String callDirectory;
   private Map<String, String> availableArguments;
   private Map<String, String> availableFlags;
   private Map<String, String> availableSubcommands;
   private List<String> examplesOfUsage;
   private boolean anonymousArgumentsAvailable;
   private List<String> anonymousArguments;
   private String callContext;

   /**
    * Constructor
    */
   public AbstractCommand() {
      this.arguments = new HashMap<>();
      this.flags = new ArrayList<>();
      this.availableArguments = new HashMap<>();
      this.availableFlags = new HashMap<>();
      this.availableSubcommands = new HashMap<>();
      this.examplesOfUsage = new ArrayList<>();
      this.anonymousArgumentsAvailable = false;
      this.anonymousArguments = new ArrayList<>();
   }

   /**
    * Getter for argument prefix
    *
    * @return argument prefix
    */
   public String getArgumentPrefix() {
      return argumentPrefix;
   }

   /**
    * Getter for flag prefix
    *
    * @return flag prefix
    */
   public String getFlagPrefix() {
      return flagPrefix;
   }

   /**
    * Getter for subcommand prefix
    *
    * @return subcommand prefix
    */
   public String getSubcommandPrefix() {
      return subcommandPrefix;
   }

   /**
    * Getter for application command name
    *
    * @return application command name
    */
   public String getApplicationCommandName() {
      return applicationCommandName;
   }

   /**
    * Setter for application command name
    *
    * @param applicationCommandName to set
    */
   public void setApplicationCommandName(String applicationCommandName) {
      this.applicationCommandName = applicationCommandName;
   }

   /**
    * Getter for the arguments
    *
    * @return map of arguments
    */
   public Map<String, String> getArguments() {
      return arguments;
   }

   /**
    * Getter for the anonymous arguments
    *
    * @return list of anonymous arguments
    */
   public List<String> getAnonymousArguments() {
      return anonymousArguments;
   }

   /**
    * Setter for the command
    *
    * @param command to set
    */
   public void setCommand(String command) {
      this.command = command;
   }

   /**
    * Getter for the command
    *
    * @return command
    */
   public String getCommand() {
      return command;
   }

   /**
    * Setter for the subcommand
    *
    * @param subcommand to set
    */
   public void setSubcommand(String subcommand) {
      this.subcommand = subcommand;
   }

   /**
    * Getter for the subcommand
    *
    * @return subcommand
    */
   public String getSubcommand() {
      return subcommand;
   }

   /**
    * Getter for the flags
    *
    * @return flags
    */
   public List<String> getFlags() {
      return flags;
   }

   /**
    * Setter for call directory
    *
    * @param callDirectory to set
    */
   public void setCallDirectory(String callDirectory) {
      this.callDirectory = callDirectory;
   }

   /**
    * Getter for call directory
    *
    * @return call directory
    */
   public String getCallDirectory() {
      return callDirectory;
   }

   /**
    * Adds available argument
    *
    * @param availableArgument to add
    * @param description       of the argument
    */
   public void addAvailableArgument(String availableArgument, String description) {
      availableArguments.put(availableArgument, description);
   }

   /**
    * Adds available flag
    *
    * @param availableFlag to add
    * @param description   of the flag
    */
   public void addAvailableFlag(String availableFlag, String description) {
      availableFlags.put(availableFlag, description);
   }

   /**
    * Adds available subcommand
    *
    * @param availableSubcommand to add
    * @param description         of the subcommand
    */
   public void addAvailableSubcommand(String availableSubcommand, String description) {
      availableSubcommands.put(availableSubcommand, description);
   }

   /**
    * Adds example of usage. You don't need to start each example with your
    * application command name and this command name. It will be added automatically.
    * So just start with arguments, flags, etc.
    * <p>
    * EXAMPLE:<p>
    * Your application and command name: "calc add"<p>
    * Your example: "calc add -a 1 -b 2"<p>
    * Input to this method: "-a 1 -b 2"
    *
    * @param exampleOfUsage to add
    * @deprecated since 3.0.0-pre
    */
   @Deprecated
   public void addExampleOfUsage(String exampleOfUsage) {
      examplesOfUsage.add(exampleOfUsage);
   }

   /**
    * Adds example of usage. You don't need to start each example with your
    * application command name and this command name. It will be added automatically.
    * So just start with arguments, flags, etc.
    * <p>
    * EXAMPLE:<p>
    * Your application and command name: "calc add"<p>
    * Your example: "calc add -a 1 -b 2"<p>
    * Input to this method: "-a", "1", "-b", "2"
    *
    * @param exampleOfUsageCommands to add
    * @deprecated since 3.0.0-pre
    */
   @Deprecated
   public void addExampleOfUsage(String... exampleOfUsageCommands) {
      StringBuilder stringBuilder = new StringBuilder();
      for (int i = 0; i < exampleOfUsageCommands.length; i++) {
         stringBuilder.append(exampleOfUsageCommands[i]).append(" ");
      }
      examplesOfUsage.add(stringBuilder.toString());
   }

   /**
    * Adds example of usage by creating the {@link ExampleOfUsageBuilder}.
    * You don't need to start each example with your
    * application command name and this command name. It will be added automatically.
    * So just start with arguments, flags, etc.
    * <p>
    * EXAMPLE:<p>
    * Your application and command name: "calc add"<p>
    * Your example: "calc add -a 1 -b 2"<p>
    * Usage: addExampleOfUsage().argument(a, 1).argument(b, 2).description("adds two numbers: 1 + 2");
    * @return new {@link ExampleOfUsageBuilder}
    */
   public ExampleOfUsageBuilder addExampleOfUsage() {
      return new ExampleOfUsageBuilder(this);
   }

   /**
    * Calls the command. If command contains the "!h" flag then
    * method displays the help description of the command, if not
    * - the run() method is called.
    */
   public void call() {
      if (flags.contains("h")) {
         Logger.log(displayHelp());
         return;
      }
      if (!handleErrors())
         run();
   }

   /**
    * Displays help for command
    *
    * @return help description
    */
   public String displayHelp() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getInfo());
      stringBuilder.append("\n\n");
      if (availableArguments.size() > 0) {
         stringBuilder.append("AVAILABLE ARGUMENTS:\n");
         iterateForEachAvailableMaps(availableArguments, stringBuilder);
         stringBuilder.append("\n");
      }
      if (availableFlags.size() > 0) {
         stringBuilder.append("AVAILABLE FLAGS:\n");
         iterateForEachAvailableMaps(availableFlags, stringBuilder);
         stringBuilder.append("\n");
      }
      if (availableSubcommands.size() > 0) {
         stringBuilder.append("AVAILABLE SUBCOMMANDS:\n");
         iterateForEachAvailableMaps(availableSubcommands, stringBuilder);
         stringBuilder.append("\n");
      }
      if (examplesOfUsage.size() > 0) {
         stringBuilder.append("EXAMPLES OF USAGE:\n");
         examplesOfUsage.forEach(example -> {
            stringBuilder.append(applicationCommandName)
               .append(" ")
               .append(command)
               .append(example)
               .append("\n");
         });
      }
      stringBuilder.append("\n");
      return stringBuilder.toString();
   }

   /**
    * Setter for availability of anonymous arguments
    *
    * @param anonymousArgumentsAvailable to set
    */
   public void setAnonymousArgumentsAvailable(boolean anonymousArgumentsAvailable) {
      this.anonymousArgumentsAvailable = anonymousArgumentsAvailable;
   }

   /**
    * Getter for availiability of anonymous arguments information
    *
    * @return availiability of anonymous arguments
    */
   public boolean isAnonymousArgumentsAvailable() {
      return anonymousArgumentsAvailable;
   }

   /**
    * Getter for call context
    *
    * @return called command with arguments
    */
   public String getCallContext() {
      return callContext;
   }

   /**
    * Setter for call context
    *
    * @param callContext to set
    */
   public void setCallContext(String callContext) {
      this.callContext = callContext;
   }

   /**
    * Sets prefixes for command
    *
    * @param argumentPrefix   to set
    * @param flagPrefix       to set
    * @param subcommandPrefix to set
    */
   public void setPrefixes(String argumentPrefix, String flagPrefix, String subcommandPrefix) {
      this.argumentPrefix = argumentPrefix;
      this.flagPrefix = flagPrefix;
      this.subcommandPrefix = subcommandPrefix;
   }

   private void iterateForEachAvailableMaps(Map<String, String> map, StringBuilder stringBuilder) {
      map.forEach((k, v) -> {
         stringBuilder.append(k);
         stringBuilder.append(" - ");
         stringBuilder.append(v);
         stringBuilder.append("\n");
      });
   }

   protected boolean handleErrors() {
      for (Map.Entry<String, String> entry : arguments.entrySet()) {
         String argumentKey = entry.getKey();
         boolean foundInAvailables = false;
         for (Map.Entry<String, String> entry1 : availableArguments.entrySet()) {
            if (argumentKey.equals(entry1.getKey())) {
               foundInAvailables = true;
            }
         }
         if (!foundInAvailables) {
            Logger.log("ERROR: '" + argumentKey + "' argument is not available");
            return true;
         }
      }
      for (String flag : flags) {
         boolean foundInAvailables = false;
         for (Map.Entry<String, String> entry1 : availableFlags.entrySet()) {
            if (flag.equals(entry1.getKey())) {
               foundInAvailables = true;
            }
         }
         if (!foundInAvailables) {
            Logger.log("ERROR: '" + flag + "' flag is not available");
            return true;
         }
      }
      if (subcommand != null) {
         boolean foundInAvailables = false;
         for (Map.Entry<String, String> entry : availableSubcommands.entrySet()) {
            if (subcommand.equals(entry.getKey())) {
               foundInAvailables = true;
            }
         }
         if (!foundInAvailables) {
            Logger.log("ERROR: '" + subcommand + "' subcommand is not available");
            return true;
         }
      }
      if (availableArguments.size() == 0 && arguments.size() > 0) {
         Logger.log("ERROR: no available arguments");
         return true;
      }
      if (availableFlags.size() == 0 && flags.size() > 0) {
         Logger.log("ERROR: no available flags");
         return true;
      }
      if (availableSubcommands.size() == 0 && subcommand != null) {
         Logger.log("ERROR: no available subcommands");
         return true;
      }
      return false;
   }

   /**
    * Returns the help description
    *
    * @return help description
    */
   protected abstract String getInfo();

   /**
    * Run method for the command
    */
   protected abstract void run();

   /**
    * Initialization of the command
    */
   public abstract void init();
}
