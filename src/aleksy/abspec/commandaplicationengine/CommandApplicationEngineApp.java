package aleksy.abspec.commandaplicationengine;

import aleksy.abspec.commandaplicationengine.exception.CaeException;
import aleksy.abspec.commandaplicationengine.runner.CaeRunner;

/**
 * Command Application Engine App
 */
public class CommandApplicationEngineApp {

   /**
    * Main method
    *
    * @param args of program
    *
    *             @throws IllegalAccessException illegal access
    *             @throws CaeException business exception
    *             @throws InstantiationException instantiation exception
    */
   public static void main(String[] args) throws IllegalAccessException, CaeException, InstantiationException {
      new CaeRunner() {
         @Override
         protected void globalRun(String args[]) {
         }
      }.run(args);
   }
}
