package aleksy.abspec.commandaplicationengine.runner;

import aleksy.abspec.commandaplicationengine.annotation.Command;
import aleksy.abspec.commandaplicationengine.constants.BasicConstants;
import aleksy.abspec.commandaplicationengine.exception.CaeException;
import aleksy.abspec.commandaplicationengine.logic.CommandsUtil;
import aleksy.abspec.commandaplicationengine.logic.Logger;
import aleksy.abspec.commandaplicationengine.model.AbstractCommand;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.List;

/**
 * Engine runner. To run the application application create the new instance of this runner
 * and call two methods: "setApplicationName()" to set information about version, name and author,
 * and "addCommand" to add own command of the application.
 */
public abstract class CaeRunner {
   private List<Class<? extends AbstractCommand>> commands;
   private String applicationName;
   private String applicationCommandName;
   private String applicationVersion;
   private String applicationAuthor;
   private String flagPrefix;
   private String argumentPrefix;
   private String subcommandPrefix;
   private String applicationPackage;
   private String applicationInformation;

   /**
    * Constructor
    */
   public CaeRunner() {
      commands = new ArrayList<>();
      applicationName = BasicConstants.NAME;
   }

   /**
    * Run method of the runner class. This method process the command from the args.
    *
    * @param args of the program
    * @throws InstantiationException for problem with instantiate the command class
    * @throws IllegalAccessException for problem with instantiate the command class
    * @throws CaeException           when command is wrong
    */
   public void run(String[] args) throws InstantiationException, IllegalAccessException, CaeException {
      globalRun(args);
      handleErrors();
      lookForCommands();
      if (args.length == 1) {
         Logger.log("Empty command. To see available commands type '" + applicationCommandName + " help'");
         return;
      }

      if (args[1].equals("help")) {
         displayHelp();
         return;
      }

      if (args[1].equals("info")) {
         displayInfo();
         return;
      }

      AbstractCommand command = CommandsUtil.split(args, commands,
         flagPrefix, argumentPrefix, subcommandPrefix, applicationCommandName);
      if (command == null) {
         Logger.log(args[1] + " is not a recognized command");
         return;
      }
      command.call();
   }

   private void handleErrors() throws CaeException {
      String message = "Please set it to runner before run method.";
      if (argumentPrefix == null)
         throw new CaeException("Argument prefix is not defined. " + message);
      if (flagPrefix == null)
         throw new CaeException("Flag prefix is not defined. " + message);
      if (subcommandPrefix == null)
         throw new CaeException("Subcommand prefix is not defined. " + message);
      if (applicationName == null)
         throw new CaeException("Name of application is not defined. " + message);
      if (applicationCommandName == null)
         throw new CaeException("Command main name is not defined. " + message);
      if (applicationVersion == null)
         throw new CaeException("Version of application is not defined. " + message);
      if (applicationAuthor == null)
         throw new CaeException("application author is not defined. " + message);
      if (applicationPackage == null)
         throw new CaeException("application package is not defined. " + message);
   }

   private void lookForCommands() {
      Reflections ref = new Reflections(applicationPackage);
      for (Class<?> cl : ref.getTypesAnnotatedWith(Command.class)) {
         commands.add(cl.asSubclass(AbstractCommand.class));
      }
   }

   protected abstract void globalRun(String args[]);

   /**
    * Adds a new command to the runner.
    *
    * @param commandClass to add
    */
   public void addCommand(Class<? extends AbstractCommand> commandClass) {
      commands.add(commandClass);
   }

   /**
    * Setter for application name
    *
    * @param applicationName to set
    */
   public void setApplicationName(String applicationName) {
      this.applicationName = applicationName;
   }

   /**
    * Setter for application author
    *
    * @param applicationAuthor to set
    */
   public void setApplicationAuthor(String applicationAuthor) {
      this.applicationAuthor = applicationAuthor;
   }

   /**
    * Setter for application command name. It is the name which is typed
    * to the console when user calls the program
    *
    * @param applicationCommandName to set
    */
   public void setApplicationCommandName(String applicationCommandName) {
      this.applicationCommandName = applicationCommandName;
   }

   /**
    * Setter for application version
    *
    * @param applicationVersion to set
    */
   public void setapplicationVersion(String applicationVersion) {
      this.applicationVersion = applicationVersion;
   }

   /**
    * Setter for flag prefix
    *
    * @param flagPrefix to set
    */
   public void setFlagPrefix(String flagPrefix) {
      this.flagPrefix = flagPrefix;
   }

   /**
    * Setter for argument prefix
    *
    * @param argumentPrefix to set
    */
   public void setArgumentPrefix(String argumentPrefix) {
      this.argumentPrefix = argumentPrefix;
   }

   /**
    * Setter for subcommand prefix
    *
    * @param subcommandPrefix to set
    */
   public void setSubcommandPrefix(String subcommandPrefix) {
      this.subcommandPrefix = subcommandPrefix;
   }

   /**
    * Setter for application package
    *
    * @param applicationPackage to set
    */
   public void setApplicationPackage(String applicationPackage) {
      this.applicationPackage = applicationPackage;
   }

   /**
    * Setter for application information
    *
    * @param applicationInformation to set
    */
   public void setApplicationInformation(String applicationInformation) {
      this.applicationInformation = applicationInformation;
   }

   private void displayHelp() {
      Logger.log("====== COMMANDS ======\n");
      for (Class<? extends AbstractCommand> command : commands) {
         Logger.log(command.getDeclaredAnnotation(Command.class).name() + " - "
            + command.getDeclaredAnnotation(Command.class).description());
      }
      Logger.log("info - displays the information about the application");
      Logger.log("help - displays names and descriptions of the commands\n");
      Logger.log("======== INFO ========\n");
      Logger.log("To call the command with the arguments type:\n"
         + applicationCommandName + " command_name " + argumentPrefix + "arg0 val0 " + argumentPrefix + "arg1 val1" +
         "\nTo add the flag to the command type:" +
         "\n" + applicationCommandName + " command_name " + flagPrefix + "flag0 " + flagPrefix + "flag1" +
         "\nTo call the command with its subcommand type:\n"
         + applicationCommandName + " command_name " + subcommandPrefix + "subcommand" +
         "\n\n\nSimple example: \n" +
         "calculator divide " + subcommandPrefix + "of-integers " + flagPrefix + "display-with-modulo " + argumentPrefix +
         "x 23 " + argumentPrefix + "y 17" +
         "\n\n\nTo display detail help description for the command type:\n"
         + applicationCommandName + " command_name " + flagPrefix + "h");
   }

   private void displayInfo() {
      Logger.log(applicationName + " " + applicationVersion + "\n" + applicationAuthor + "\n");
      if (applicationInformation != null)
         Logger.log(applicationInformation);
      Logger.log("Application runs on " + BasicConstants.NAME + " " + BasicConstants.VERSION +
         " created by: " + BasicConstants.AUTHOR);
   }
}
