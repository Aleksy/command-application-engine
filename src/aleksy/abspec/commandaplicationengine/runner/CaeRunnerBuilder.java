package aleksy.abspec.commandaplicationengine.runner;

/**
 * Main builder for {@link CaeRunner}
 */
public class CaeRunnerBuilder {
   private String name;
   private String version;
   private String author;

   /**
    * Sets basic information about the application to runner.
    *
    * @param name    to set
    * @param version to set
    * @param author  to set
    * @return detail builder
    */
   public DetailBuilder createForApplication(String name, String version, String author) {
      this.name = name;
      this.version = version;
      this.author = author;
      return new DetailBuilder(name, version, author);
   }
}
