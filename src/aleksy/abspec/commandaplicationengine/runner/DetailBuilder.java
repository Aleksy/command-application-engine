package aleksy.abspec.commandaplicationengine.runner;

import aleksy.abspec.commandaplicationengine.constants.Prefixes;

/**
 * Detail builder for {@link CaeRunner}
 */
public class DetailBuilder {
   private String name;
   private String version;
   private String author;
   private String commandName;
   private String flagPrefix;
   private String argumentPrefix;
   private String subcommandPrefix;
   private String applicationPackage;
   private String applicationInformation;

   /**
    * The constructor
    *
    * @param name    of application
    * @param version of application
    * @param author  of application
    */
   public DetailBuilder(String name, String version, String author) {
      this.name = name;
      this.version = version;
      this.author = author;
   }

   /**
    * Setter for command name
    *
    * @param commandName to set
    * @return builder
    */
   public DetailBuilder withCommandName(String commandName) {
      this.commandName = commandName;
      return this;
   }

   /**
    * Setter for flag prefix
    *
    * @param flagPrefix to set
    * @return builder
    */
   public DetailBuilder withFlagPrefix(String flagPrefix) {
      this.flagPrefix = flagPrefix;
      return this;
   }

   /**
    * Setter for subcommand prefix
    *
    * @param subcommandPrefix to set
    * @return builder
    */
   public DetailBuilder withSubcommandPrefix(String subcommandPrefix) {
      this.subcommandPrefix = subcommandPrefix;
      return this;
   }

   /**
    * Setter for argument prefix
    *
    * @param argumentPrefix to set
    * @return builder
    */
   public DetailBuilder withArgumentPrefix(String argumentPrefix) {
      this.argumentPrefix = argumentPrefix;
      return this;
   }

   /**
    * Setter for application package
    *
    * @param applicationPackage to set
    * @return builder
    */
   public DetailBuilder withPackage(String applicationPackage) {
      this.applicationPackage = applicationPackage;
      return this;
   }

   /**
    * Setter for application information
    *
    * @param applicationInformation to set
    * @return builder
    */
   public DetailBuilder withInformation(String applicationInformation) {
      this.applicationInformation = applicationInformation;
      return this;
   }

   /**
    * Sets default prefixes for runner (see: {@link Prefixes})
    *
    * @return builder
    */
   public DetailBuilder withDefaultPrefixes() {
      this.argumentPrefix = Prefixes.DEFAULT_ARGUMENT_PREFIX;
      this.flagPrefix = Prefixes.DEFAULT_FLAG_PREFIX;
      this.subcommandPrefix = Prefixes.DEFAULT_SUBCOMMAND_PREFIX;
      return this;
   }

   /**
    * Fills template class (which has overwritten the globalRun method) with
    * all fields.
    *
    * @param template to fill
    * @return filled {@link CaeRunner}
    */
   public CaeRunner fill(CaeRunner template) {
      template.setApplicationInformation(applicationInformation);
      template.setApplicationPackage(applicationPackage);
      template.setArgumentPrefix(argumentPrefix);
      template.setFlagPrefix(flagPrefix);
      template.setSubcommandPrefix(subcommandPrefix);
      template.setApplicationCommandName(commandName);
      template.setApplicationAuthor(author);
      template.setApplicationName(name);
      template.setapplicationVersion(version);
      return template;
   }
}
