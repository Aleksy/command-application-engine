package aleksy.abspec.commandaplicationengine.logic;

import aleksy.abspec.commandaplicationengine.annotation.Command;
import aleksy.abspec.commandaplicationengine.exception.CaeException;
import aleksy.abspec.commandaplicationengine.model.AbstractCommand;

import java.util.List;

/**
 * Commands util class
 */
public class CommandsUtil {
   /**
    * Splits the arguments to the new command instance
    *
    * @param args                   of the program
    * @param commands               list
    * @param argumentPrefix         argument prefix
    * @param flagPrefix             flag prefix
    * @param subcommandPrefix       subcommand prefix
    * @param applicationCommandName command name of application
    * @return new instance of command or null when is not recognized
    * @throws IllegalAccessException when newInstance() method of the command class throws an exception
    * @throws InstantiationException when newInstance() method of the command class throws an exception
    * @throws CaeException           when command syntax is wrong
    */
   public static AbstractCommand split(String[] args,
                                       List<Class<? extends AbstractCommand>> commands,
                                       String flagPrefix,
                                       String argumentPrefix,
                                       String subcommandPrefix,
                                       String applicationCommandName)
      throws IllegalAccessException, InstantiationException, CaeException {
      AbstractCommand commandInstance = lookForCommandInstance(args, commands, argumentPrefix, flagPrefix, subcommandPrefix);

      StringBuilder callContextBuilder = new StringBuilder();
      boolean subcommandExists = false;
      if (commandInstance != null) {
         commandInstance.setApplicationCommandName(applicationCommandName);
         for (int i = 1; i < args.length; i++) {
            String arg = args[i];
            callContextBuilder.append(arg).append(" ");
            boolean collected = collectSubcommand(arg, subcommandPrefix, subcommandExists, commandInstance);
            if (collected) subcommandExists = true;
            if (!collected && !commandInstance.isAnonymousArgumentsAvailable())
               collected = collectArguments(arg, args, argumentPrefix, i, flagPrefix, commandInstance);
            if (!collected)
               collected = collectFlag(arg, flagPrefix, commandInstance);
            if (!collected && commandInstance.isAnonymousArgumentsAvailable()) {
               collectAnonymousArguments(arg, i, commandInstance);
            }
         }
         commandInstance.setCallDirectory(args[0]);
         commandInstance.setCallContext(callContextBuilder.toString());
      }
      return commandInstance;
   }

   private static void collectAnonymousArguments(String arg, int argIndex, AbstractCommand commandInstance) {
      if (argIndex != 1) {
         commandInstance.getAnonymousArguments().add(arg);
      }
   }

   private static boolean collectFlag(String arg, String flagPrefix, AbstractCommand commandInstance) {
      if (arg.startsWith(flagPrefix)) {
         commandInstance.getFlags().add(arg.substring(flagPrefix.length()));
         return true;
      }
      return false;
   }

   private static boolean collectArguments(String arg, String[] args, String argumentPrefix, int argIndex, String flagPrefix, AbstractCommand commandInstance) throws CaeException {
      if (arg.startsWith(argumentPrefix)) {
         if (argIndex == args.length - 1)
            throw new CaeException("No value for parameter '" + arg.substring(argumentPrefix.length()) + "'");
         if (args[argIndex + 1].startsWith(argumentPrefix) || args[argIndex + 1].startsWith(flagPrefix))
            throw new CaeException("No value for parameter '" + arg.substring(argumentPrefix.length()) + "'");
         commandInstance.getArguments().put(arg.substring(argumentPrefix.length()), args[argIndex + 1]);
         return true;
      }
      return false;
   }

   private static boolean collectSubcommand(String arg, String subcommandPrefix, boolean subcommandExists, AbstractCommand commandInstance) throws CaeException {
      if (arg.startsWith(subcommandPrefix) && subcommandExists) {
         throw new CaeException("Only one subcommand can be added.");
      }
      if (arg.startsWith(subcommandPrefix) && !subcommandExists) {
         commandInstance.setSubcommand(arg.substring(subcommandPrefix.length()));
         return true;
      }
      return false;
   }

   private static AbstractCommand lookForCommandInstance(String[] args,
                                                         List<Class<? extends AbstractCommand>> commands,
                                                         String argumentPrefix,
                                                         String flagPrefix,
                                                         String subcommandPrefix)
      throws IllegalAccessException, InstantiationException {
      AbstractCommand commandInstance = null;
      for (Class<? extends AbstractCommand> command : commands) {
         String value = command.getDeclaredAnnotation(Command.class).name();
         if (value.equals(args[1])) {
            commandInstance = command.newInstance();
            commandInstance.setPrefixes(argumentPrefix, flagPrefix, subcommandPrefix);
            commandInstance.setCommand(args[1]);
            commandInstance.init();
         }
      }
      return commandInstance;
   }
}
