# Command Application Engine

It is the implementation of original engine for command applications. The CAE contains
the special, configurable runner, who loads the commands which were set to the application.

## Installation

Download the [CAE JAR library](https://drive.google.com/open?id=1ckZUN4l3k3oVFOq8tkwdxqNa-mfU5CTW) and attach it as extended library to your project.  
Actual version: 3.0.0-pre.

## Workflow

To run the engine write simple class with main method like here:

```java

   public static void main(String... args) throws IllegalAccessException, CaeException, InstantiationException {
           // new instance of builder of runner:
      new CaeRunnerBuilder()
           // your app information - name, version and author:
         .createForApplication("Example Application", "1.0.0", "You")
           // command name of your application (you will call your app by "example <command>"
         .withCommandName("example")
           // default package with classes annotated by Command annotation:
         .withPackage("package.with.command.classes")
           // Prefix of the argument:
         .withArgumentPrefix("-")
           // prefix of the flag:
         .withFlagPrefix("/")
           // prefix of the subcommand:
         .withSubcommandPrefix(":")
           // or use this if you don't want to change default prefixes (-, /, :):
         .withDefaultPrefixes()
           // description of your application which will be displayed when you type "example help":
         .withInformation("This is my app.")
           // fill your new CaeRunner with given data:
         .fill(new CaeRunner() {
            @Override
            protected void globalRun(String[] strings) {
               System.out.println("This text will be display each time you start the application. " +
                  "You can leave this method empty or use it in own way");
            }
            // running the runner:
         }).run(args);
   }

```

## Installation of your command application

1. Create new folder somewhere in disk.
2. Add this folder to PATH environment variable.
3. Compile your application to JAR library and put it to created folder.
4. In this folder create new BASH file and name it like the command name of your application ("ex.bat" in example above)
5. Write this command into this file:
```
@echo off
java -jar "C:\PATH\TO\CREATED_FOLDER\example-application.jar" %CD% %*
```
6. Now you can go to the cmd and try the application by typing:
```
ex info
```
You should see:
```
Example Application 1.0.0
You

This is my app.

Application runs on Command Application Engine 3.0.0-pre created by: Aleksy Bernat, Wroclaw
```

As you can see the engine requires an actual directory (%CD%) as a first argument of your application.
This directory can be accessed in the command class by getCallDirectory(). Sometimes it can be useful
for the developer.

## Adding new command

To add your own command you need to create new class. Let's write "hello world" command.
New class:
```java

@Command(name = "hello", description = "writes 'hello world'")
public class HelloCommand extends AbstractCommand {

   @Override
   protected String getInfo() {
      return "Writes hello world in the console.";
   }

   @Override
   protected void run() {
      System.out.println("hello world");
   }

   @Override
   protected void init() {
   }
}

```

Now you can override the JAR file in created earlier folder and type in cmd:
```
ex hello
```

You should see simple:
```
hello world
```
Command Application Engine automaticly loads new added command (it finds it by "Command" annotation).
Your class should extend the AbstractCommand class.
You can write "ex help" to see:

```
====== COMMANDS ======

hello - writes 'hello world'
info - displays the information about the application
help - displays names and descriptions of the commands

======== INFO ========

To call the command with the arguments type:
im command_name -arg0 val0 -arg1 val1
To add the flag to the command type:
im command_name /flag0 /flag1
To call the command with its subcommand type:
im command_name :subcommand


Simple example:
calculator divide :of-integers /display-with-modulo -x 23 -y 17


To display detail help description for the command type:
im command_name /h
```

And "ex hello /h" to see:
```
Writes hello world in the console.
```

## Commands with parameters

To add parameters to the command you need to place available arguments, flags and subcommands
to method init(). In example if you would like to create a simple calculator you can create a class like this:
```java
@Command(name = "add", description = "adds two numbers")
public class HelloCommand extends AbstractCommand {

   @Override
   protected String getInfo() {
      return "Adds two numbers given in arguments.";
   }

   @Override
   protected void run() {
      float result;
      if ("float".equals(getSubcommand())) {
         float a = Float.parseFloat(getArguments().get("a"));
         float b = Float.parseFloat(getArguments().get("b"));
         result = a + b;
      } else {
         int a = Integer.parseInt(getArguments().get("a"));
         int b = Integer.parseInt(getArguments().get("b"));
         result = a + b;
      }
      System.out.println(result);
      String fileFlag = getFlags().get(0);
      if (fileFlag != null)
         CalcUtils.saveToFile(result);
   }

   @Override
   public void init() {
      addAvailableArgument("a", "first number");
      addAvailableArgument("b", "second number");
      addAvailableFlag("file", "saves a result to a file result.txt");
      addAvailableSubcommand("float", "runs adding two float numbers");
      addExampleOfUsage().argument("a", "12").argument("b", "13").flag("file")
         .description("adds 12 and 13, and saves the result to file");
      addExampleOfUsage().subcommand("float").argument("a", "12.13")
         .argument("b", "13.12").description("adds 12.13 and 13.12 in float precision");
   }

   @Override
   protected boolean handleErrors() {
      if (!super.handleErrors())
         if (getArguments().size() < 2) {
            System.out.println("Not enough arguments!");
            return true;
         }
      return false;
   }
}
```

To this command you can give two parameters, one flag and one subcommand.
Super method handleErrors() looks for basic errors like wrong names of arguments,
flags, etc. so you should use it. Overridden method checks business errors (in this example: not enough
arguments).
You can also enable anonymous arguments by setting:
```java
setAnonymousArgumentsAvailable(true);
```
in the init() method. Now you can decide on your own how to handle the arguments.
For instance you can make the adding method callable not in this way:
```java
example add -a 12 -b 13
```
but shorter:
```java
example add 12 13
```

That will be updated class:
```java
@Command(name = "add", description = "adds two numbers")
public class HelloCommand extends AbstractCommand {

   @Override
   protected String getInfo() {
      return "Adds two numbers given in arguments.";
   }

   @Override
   protected void run() {
      float result;
      int a = Integer.parseInt(getAnonymousArguments().get(0));
      int b = Integer.parseInt(getAnonymousArguments().get(1));
      result = a + b;
      System.out.println(result);
   }

   @Override
   public void init() {
      setAnonymousArgumentsAvailable(true);
      addExampleOfUsage().description("I forgot to add the proper method to create" +
         "an anonymous argument, sorry please don't kill me. I'll add it in 3.0.1-pre");
   }

   @Override
   protected boolean handleErrors() {
      if (!super.handleErrors())
         if (getAnonymousArguments().size() < 2) {
            System.out.println("Not enough arguments!");
            return true;
         }
      return false;
   }
}
```